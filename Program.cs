﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OS_laba_6
{
    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());

        }

        public static string[] LoadData()
        {
            if (!File.Exists("user_data.csv"))
                File.Create("user_data.csv").Close();
            if (!File.Exists("speedLog.csv"))
                File.Create("speedLog.csv").Close();
            using (StreamReader sr = new StreamReader("user_data.csv", Encoding.UTF8))
            {
                string[] data = new string[3];
                for (int i = 0; i < 3; i++)
                {
                    data[i] = sr.ReadLine();
                }
                return data;
            }
        }

    }
}
