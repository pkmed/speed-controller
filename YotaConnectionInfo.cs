﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Windows.Forms.VisualStyles;
using Newtonsoft.Json;

namespace OS_laba_6
{
    class YotaConnectionInfo
    {
        public static YotaStatusData GetStatusData()
        {
            WebClient client = new WebClient();
            using (Stream data = client.OpenRead("http://status.yota.ru/xml_action.cgi?method=get&module=duster&file=json_status") ?? throw new Exception("Connection problems"))
            {
                using (StreamReader reader = new StreamReader(data))
                {
                    YotaStatusData result = JsonConvert.DeserializeObject<YotaStatusData>(reader.ReadToEnd());
                    return result;
                }
            }
        }

        public static string CheckPings()
        {
            StringBuilder response = new StringBuilder();
            List<string> serversList = new List<string>();
            //serversList.Add("yandex.ru");
            serversList.Add("google.com");

            Ping ping = new Ping();
            PingReply pingReply = null;

            foreach (string server in serversList)
            {
                pingReply = ping.Send(server);

                if (pingReply.Status != IPStatus.TimedOut)
                {
                    response.Append(server+"("); //server
                    response.Append(pingReply.Address+"):"); //IP
                    response.Append(pingReply.Status+" "); //Статус
                    response.Append("ping:"+pingReply.RoundtripTime + " "); //Время ответа
                    response.Append("TTL:"+pingReply.Options.Ttl + " "); //TTL
                    response.Append(pingReply.Buffer.Length+"\n"); //Размер буфера
                }
                else
                {
                    response.Append(server); //server
                    response.Append(pingReply.Status + "\n");
                }
            }

            return response.ToString();
        }
    }

    class YotaStatusData
    {
        public string Status { get; set; }
        public string Cell_ID { get; set; }
        public int SINR { get; set; }
        public int RSRP { get; set; }
        public int current_speed_rx { get; set; }
        public int current_speed_tx { get; set; }
    }
}
