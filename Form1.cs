﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Text;
using System.Threading;
using System.Timers;
using System.Windows.Forms;
using Timer = System.Timers.Timer;

namespace OS_laba_6
{
    public partial class Form1 : Form
    {
        private const string Url = "my.yota.ru/selfcare/login";
        private const string OpenedChromeTab = "Новая вкладка";
        private const string LoginPageHeaderEn = "Yota - Log in to your account";
        private const string LoginPageHeaderRu = "Личный кабинет Yota — вход по номеру телефона/регистрация";
        private const string SpeedPageHeader = "Yota 4G";

        //TAB's press count
        public const int LOGIN_FIELD = 3;
        public const int PASS_FIELD = 1;
        public const int SLIDER = 10;
        public const int ACCEPT_BTN = 3;

        private Timer YotaStatusDataRequestTimer = new Timer();
        private Timer SpeedSwitchTimer = new Timer();
        private Timer TimeLeftTimer = new Timer();

        //for left time count
        DateTime end;

        delegate void _UpdatePingsWindow(string row);

        delegate void _UpdateTimeLeftLabel(string row);

        public Form1()
        {
            InitializeComponent();

            YotaStatusDataRequestTimer.Interval = 1000;
            YotaStatusDataRequestTimer.Elapsed += YotaStatusDataRequestTimerElapsed;
            YotaStatusDataRequestTimer.Start();
            
            string[] data = Program.LoadData();
            loginTextField.Text = data[0];
            passTextField.Text = data[1];
            speedList.SelectedIndex = Convert.ToInt32(data[2]);
            lastSettedSpeedLabel.Text = speedList.Items[Convert.ToInt32(data[2])].ToString();
        }

        private void YotaStatusDataRequestTimerElapsed(object sender, ElapsedEventArgs e)
        {
            try
            {
                YotaStatusData yotaStatusData = YotaConnectionInfo.GetStatusData();

                CellIDLabel.Invoke(new Action<string>(s => CellIDLabel.Text = s),yotaStatusData.Cell_ID);
                SINRLabel.Invoke(new Action<string>(s => SINRLabel.Text = s),yotaStatusData.SINR.ToString());
                RSRPLabel.Invoke(new Action<string>(s => RSRPLabel.Text = s),(yotaStatusData.RSRP-141).ToString());
                CurrentSpeedLabel.Invoke(new Action<string>(s => CurrentSpeedLabel.Text = s),yotaStatusData.current_speed_rx/1000 + "/" + yotaStatusData.current_speed_tx/1000);

                if (ShowPingsFlag.Checked)
                {
                    _UpdatePingsWindow pings = UpdatePingsWindow;
                    PingReportWindow.Invoke(pings, YotaConnectionInfo.CheckPings());
                }
            }
            catch (Exception exc)
            {
                CellIDLabel.Invoke(new Action<string>(s => CellIDLabel.Text = s), exc.Message);
                SINRLabel.Invoke(new Action(() => SINRLabel.Text = ""));
                RSRPLabel.Invoke(new Action(() => RSRPLabel.Text = ""));
                CurrentSpeedLabel.Invoke(new Action(() => CurrentSpeedLabel.Text = ""));
            }
        }

        private void UpdatePingsWindow(string row)
        {
            PingReportWindow.Items.Add(row);
            PingReportWindow.SetSelected(PingReportWindow.Items.Count - 1, true);
        }

        private void UpdateTimeLeftLabel(string time)
        {
            TimeLeftLabel.Text = time;
        }

        private void ChangeSpeedBtn_Click(object sender, EventArgs e)
        {
            if (timerInterval.Text != "" && isTimerUsing.Checked)
            {
                StartSpeedSwitchTimer();
                StartTimeLeftTimer();
            }
            ChangeSpeed(loginTextField.Text, passTextField.Text, Convert.ToInt32(speedList.SelectedIndex));
        }

        private void DelayedStartBtn_Click(object sender, EventArgs e)
        {
            TimeLeftLabel.Text = timerInterval.Text;
            StartSpeedSwitchTimer();
            StartTimeLeftTimer();
        }

        private void StartSpeedSwitchTimer()
        {
            int hoursInMs = Convert.ToInt32(timerInterval.Text.Split(':')[0]) * 3600000;
            int minutesInMs = Convert.ToInt32(timerInterval.Text.Split(':')[1]) * 60000;
            int intervalTotal = hoursInMs + minutesInMs;
            SpeedSwitchTimer.Interval = intervalTotal;
            SpeedSwitchTimer.Elapsed += SpeedSwitchTimerElapsed;
            SpeedSwitchTimer.Start();
        }

        private void SpeedSwitchTimerElapsed(object sender, ElapsedEventArgs e)
        {
            int speed = Convert.ToInt32(speedListTimer.Invoke(new Func<int>(() => speedListTimer.SelectedIndex)));
            string login = speedListTimer.Invoke(new Func<string>(() => loginTextField.Text)).ToString();
            string pass = speedListTimer.Invoke(new Func<string>(() => passTextField.Text)).ToString();
            ChangeSpeed(login, pass, speed);
            SpeedSwitchTimer.Stop();
        }

        private void StartTimeLeftTimer()
        {
            TimeLeftTimer.Interval = 1000;
            TimeLeftTimer.Elapsed += TimeLeftTimerElapsed;

            int hour = Convert.ToInt32(timerInterval.Text.Split(':')[0]);
            int min = Convert.ToInt32(timerInterval.Text.Split(':')[1]);
            end = DateTime.Now.AddHours(hour);
            end = end.AddMinutes(min);

            TimeLeftTimer.Start();
        }
        
        private void TimeLeftTimerElapsed(object sender, ElapsedEventArgs e)
        {
            _UpdateTimeLeftLabel del = UpdateTimeLeftLabel;
            TimeSpan timeLeft = end - DateTime.Now;
            if (timeLeft.Seconds < 0)
            {
                TimeLeftTimer.Stop();
            }
            else
            {
                TimeLeftLabel.Invoke(del, timeLeft.ToString().Remove(timeLeft.ToString().Length-8));
            }
        }

        private void ChangeSpeed(string login, string password, int speed)
        {
            Process chromeWindow = new Process();
            chromeWindow.StartInfo.FileName="C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe";
            chromeWindow.Start();
            UserImitationFunctions.SwitchKeyboardLayout();
            Application.DoEvents();
            WaitForHeader(OpenedChromeTab);
            Url.Normalize(NormalizationForm.FormC);
            SendKeys.SendWait(Url);
            Application.DoEvents();
            UserImitationFunctions.PressKey(UserImitationFunctions.ENTER_BTN);
            WaitForHeader(LoginPageHeaderRu, LoginPageHeaderEn);
            UserImitationFunctions.ChoosePageElement(LOGIN_FIELD);
            SendKeys.SendWait(login.Normalize(NormalizationForm.FormC));
            Application.DoEvents();
            UserImitationFunctions.ChoosePageElement(PASS_FIELD);
            SendKeys.SendWait(password.Normalize(NormalizationForm.FormC));
            Application.DoEvents();
            UserImitationFunctions.PressKey(UserImitationFunctions.ENTER_BTN);
            WaitForHeader(SpeedPageHeader);
            UserImitationFunctions.ChoosePageElement(SLIDER);
            UserImitationFunctions.SetSpeed(speed);
            UserImitationFunctions.ChoosePageElement(ACCEPT_BTN);
            UserImitationFunctions.PressKey(UserImitationFunctions.ENTER_BTN);
            Thread.Sleep(3500);
            UserImitationFunctions.PressKeyCombination(UserImitationFunctions.ALT_BTN, UserImitationFunctions.F4_BTN);
            if (speedList.InvokeRequired)
            {
                lastSettedSpeedLabel.Text = speedList.Items[speed].ToString();
                TimeLeftLabel.Invoke(new Action(() => TimeLeftLabel.Text=""));
            }
            else
            {
                lastSettedSpeedLabel.Text = speedList.Items[speed].ToString();
                TimeLeftLabel.Text = "";
            }
            LogSpeed(Convert.ToInt32(speedList.Items[speed]));
        }

        private void WaitForHeader(string header)
        {
            while (true)
            {
                if (CheckWindowHeader(header))
                {
                    Thread.Sleep(3000);
                    break;
                }
            }
        }

        private void WaitForHeader(string header, string altHeader)
        {
            while (true)
            {
                if (CheckWindowHeader(header) || CheckWindowHeader(altHeader))
                {
                    Thread.Sleep(3000);
                    break;
                }
            }
        }

        private bool CheckWindowHeader(string header)
        {
            try
            {
                IntPtr winHandle = UserImitationFunctions.GetWindow((IntPtr)Invoke(new Func<IntPtr>(() => Handle)), UserImitationFunctions.GetWindowType.GW_HWNDFIRST);
                while (winHandle != IntPtr.Zero)
                {
                    int len = UserImitationFunctions.GetWindowTextLength(winHandle);
                    StringBuilder sb_windowText = new StringBuilder(len + 1);
                    UserImitationFunctions.GetWindowText(winHandle, sb_windowText, sb_windowText.Capacity);
                    if (sb_windowText.ToString().Contains(header))
                    {
                        return true;
                    }
                    winHandle = UserImitationFunctions.GetWindow(winHandle, UserImitationFunctions.GetWindowType.GW_HWNDNEXT);
                }
                return false;
            }
            catch (Exception ee)
            {
                return false;
            }
        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            if (WindowState==FormWindowState.Minimized)
            {
                notifyIcon1.Visible = true;
                Hide();
            }
        }

        private void notifyIcon1_MouseClick(object sender, MouseEventArgs e)
        {
            Show();
            WindowState = FormWindowState.Normal;
            notifyIcon1.Visible = false;
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            using (StreamWriter sw = new StreamWriter("user_data.csv",false))
            {
                sw.WriteLine(loginTextField.Text);
                sw.WriteLine(passTextField.Text);
                sw.WriteLine(speedList.SelectedIndex);
            }
            LogSpeed(Convert.ToInt32(speedList.Items[speedList.SelectedIndex]));
        }

        private void LogSpeed(int speedValue)
        {
            using (StreamWriter sw = new StreamWriter("speedLog.csv",true))
            {
                sw.WriteLine(DateTime.UtcNow+"-"+speedValue);
            }
        }
    }
}