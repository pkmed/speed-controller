﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;

namespace OS_laba_6
{
    public class UserImitationFunctions
    {
        [DllImport("user32.dll", SetLastError = true)]
        public static extern void keybd_event(byte bVk, byte bScan, uint dwFlags, int dwExtraInfo);
        // эмуляция действий пользователя
        // нажатие клавиш на клавиатуре

        [DllImport("user32.dll", CharSet = CharSet.Unicode, SetLastError = false)]
        public static extern IntPtr SendMessage(IntPtr hWnd, UInt32 Msg, IntPtr wParam, IntPtr lParam);
        // отправка сообщения окну

        [DllImport("USER32.DLL", CharSet = CharSet.Unicode)]
        public static extern IntPtr FindWindow(string lpClassName, string lpWindowName);
        // поиск окна по имени класса окна и имени окна

        [DllImport("user32.dll", SetLastError = true)]
        public static extern int GetWindowTextLength(IntPtr hwnd);
        // получение длины имени окна 

        [DllImport("user32.dll", CharSet = CharSet.Unicode, SetLastError = true)]
        public static extern int GetWindowText(IntPtr hWnd, StringBuilder lpString, int nMaxCount);
        // получение имени окна

        [DllImport("user32.dll", CharSet = CharSet.Unicode, SetLastError = true)]
        public static extern int GetClassName(IntPtr hWnd, StringBuilder lpString, int nMaxCount);
        // получение имени класса окна

        [DllImport("user32.dll", SetLastError = true)]
        public static extern IntPtr GetWindow(IntPtr hWnd, GetWindowType uCmd);
        // поиск окон в MS Windows
        
        public enum GetWindowType : uint
        {
            GW_HWNDFIRST = 0, // найти самое первое окно
            GW_HWNDLAST = 1,
            GW_HWNDNEXT = 2, // найти следующее окно
            GW_HWNDPREV = 3,
            GW_OWNER = 4,
            GW_CHILD = 5, // найти первое дочернее окно
            GW_ENABLEDPOPUP = 6
        }

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool SetForegroundWindow(IntPtr hWnd);

        [DllImport("user32.dll", SetLastError = true)]
        static extern uint GetWindowThreadProcessId(IntPtr hWnd, out uint lpdwProcessId);

        [DllImport("user32.dll")]
        static extern IntPtr GetKeyboardLayout(uint thid);

        [DllImport("user32.dll", SetLastError = true)]
        internal static extern IntPtr ActivateKeyboardLayout(IntPtr hkl, uint Flags);

        public const int TAB_BTN = 0x09;
        public const int ENTER_BTN = 0x0D;
        public const int LEFT_ARROW = 0x25;
        public const int RIGHT_ARROW = 0x27;
        public const int ALT_BTN = 0x12;
        public const int F4_BTN = 0x73;

        public static void ChoosePageElement(int elementId)
        {
            for (int i = 0; i < elementId; i++)
            {
                PressKey(TAB_BTN);
            }
        }

        public static void SetSpeed(int speedValue)
        {
            for (int i = 0; i < 14; i++)
                PressKey(LEFT_ARROW);
            if (speedValue != 0)
            {
                for (int i = 0; i < speedValue; i++)
                {
                    PressKey(RIGHT_ARROW);
                }
            }
        }

        public static void PressKey(byte keyCode)
        {
            keybd_event((byte)keyCode, 0, 0, 0);
            keybd_event((byte)keyCode, 0, 0x2, 0);
        }

        public static void PressKeyCombination(int key1, int key2)
        {
            keybd_event((byte)key1, 0, 0, 0);
            keybd_event((byte)key2, 0, 0, 0);
            keybd_event((byte)key2, 0, 0x2, 0);
            keybd_event((byte)key1, 0, 0x2, 0);
        }

        public static void SwitchKeyboardLayout()
        {
            Process pr = Process.GetProcessesByName("chrome")[0];
            IntPtr hwnd = pr.MainWindowHandle;

            //получение идентификатора потока целевого окна
            uint dummy = 0;
            uint thid = GetWindowThreadProcessId(hwnd, out dummy);

            //смена раскладки клавиатуры на раскладку целевого окна
            IntPtr id = GetKeyboardLayout(thid);
            ActivateKeyboardLayout(id, 0);

            SetForegroundWindow(hwnd);//передаем фокус окну
            
        }
    }
}