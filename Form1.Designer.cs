﻿namespace OS_laba_6
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.label1 = new System.Windows.Forms.Label();
            this.loginTextField = new System.Windows.Forms.TextBox();
            this.passTextField = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.speedList = new System.Windows.Forms.ComboBox();
            this.ChangeSpeedBtn = new System.Windows.Forms.Button();
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.label4 = new System.Windows.Forms.Label();
            this.timerInterval = new System.Windows.Forms.MaskedTextBox();
            this.isTimerUsing = new System.Windows.Forms.CheckBox();
            this.speedListTimer = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.DelayedStartBtn = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.lastSettedSpeedLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.PingReportWindow = new System.Windows.Forms.ListBox();
            this.ShowPingsFlag = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.TimeLeftLabel = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.CellIDLabel = new System.Windows.Forms.Label();
            this.SINRLabel = new System.Windows.Forms.Label();
            this.RSRPLabel = new System.Windows.Forms.Label();
            this.CurrentSpeedLabel = new System.Windows.Forms.Label();
            this.statusStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Login:";
            // 
            // loginTextField
            // 
            this.loginTextField.Location = new System.Drawing.Point(75, 10);
            this.loginTextField.Name = "loginTextField";
            this.loginTextField.Size = new System.Drawing.Size(136, 20);
            this.loginTextField.TabIndex = 1;
            // 
            // passTextField
            // 
            this.passTextField.Location = new System.Drawing.Point(75, 36);
            this.passTextField.Name = "passTextField";
            this.passTextField.PasswordChar = '*';
            this.passTextField.Size = new System.Drawing.Size(136, 20);
            this.passTextField.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 39);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Password:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 65);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Speed:";
            // 
            // speedList
            // 
            this.speedList.FormattingEnabled = true;
            this.speedList.Items.AddRange(new object[] {
            "Free",
            "819",
            "1",
            "1.2",
            "1.5",
            "1.8",
            "2.1",
            "2.8",
            "3.5",
            "4.2",
            "5",
            "6.1",
            "7.2",
            "10",
            "Unlimit"});
            this.speedList.Location = new System.Drawing.Point(75, 62);
            this.speedList.Name = "speedList";
            this.speedList.Size = new System.Drawing.Size(136, 21);
            this.speedList.TabIndex = 6;
            // 
            // ChangeSpeedBtn
            // 
            this.ChangeSpeedBtn.Location = new System.Drawing.Point(16, 89);
            this.ChangeSpeedBtn.Name = "ChangeSpeedBtn";
            this.ChangeSpeedBtn.Size = new System.Drawing.Size(195, 43);
            this.ChangeSpeedBtn.TabIndex = 7;
            this.ChangeSpeedBtn.Text = "Change speed";
            this.ChangeSpeedBtn.UseVisualStyleBackColor = true;
            this.ChangeSpeedBtn.Click += new System.EventHandler(this.ChangeSpeedBtn_Click);
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
            this.notifyIcon1.Text = "notifyIcon1";
            this.notifyIcon1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.notifyIcon1_MouseClick);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(214, 12);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(82, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Set speed after:";
            // 
            // timerInterval
            // 
            this.timerInterval.Location = new System.Drawing.Point(302, 9);
            this.timerInterval.Mask = "00:00";
            this.timerInterval.Name = "timerInterval";
            this.timerInterval.Size = new System.Drawing.Size(84, 20);
            this.timerInterval.TabIndex = 9;
            this.timerInterval.ValidatingType = typeof(System.DateTime);
            // 
            // isTimerUsing
            // 
            this.isTimerUsing.AutoSize = true;
            this.isTimerUsing.Location = new System.Drawing.Point(217, 63);
            this.isTimerUsing.Name = "isTimerUsing";
            this.isTimerUsing.Size = new System.Drawing.Size(70, 17);
            this.isTimerUsing.TabIndex = 10;
            this.isTimerUsing.Text = "Use timer";
            this.isTimerUsing.UseVisualStyleBackColor = true;
            // 
            // speedListTimer
            // 
            this.speedListTimer.FormattingEnabled = true;
            this.speedListTimer.Items.AddRange(new object[] {
            "Free",
            "819",
            "1",
            "1.2",
            "1.5",
            "1.8",
            "2.1",
            "2.8",
            "3.5",
            "4.2",
            "5",
            "6.1",
            "7.2",
            "10",
            "Unlimit"});
            this.speedListTimer.Location = new System.Drawing.Point(261, 35);
            this.speedListTimer.Name = "speedListTimer";
            this.speedListTimer.Size = new System.Drawing.Size(125, 21);
            this.speedListTimer.TabIndex = 12;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(214, 38);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Speed:";
            // 
            // DelayedStartBtn
            // 
            this.DelayedStartBtn.Location = new System.Drawing.Point(217, 89);
            this.DelayedStartBtn.Name = "DelayedStartBtn";
            this.DelayedStartBtn.Size = new System.Drawing.Size(169, 42);
            this.DelayedStartBtn.TabIndex = 13;
            this.DelayedStartBtn.Text = "Delayed start";
            this.DelayedStartBtn.UseVisualStyleBackColor = true;
            this.DelayedStartBtn.Click += new System.EventHandler(this.DelayedStartBtn_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel,
            this.toolStripStatusLabel1,
            this.lastSettedSpeedLabel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 400);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(400, 22);
            this.statusStrip1.TabIndex = 14;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel
            // 
            this.toolStripStatusLabel.Name = "toolStripStatusLabel";
            this.toolStripStatusLabel.Size = new System.Drawing.Size(100, 17);
            this.toolStripStatusLabel.Text = "Last setted speed:";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(0, 17);
            // 
            // lastSettedSpeedLabel
            // 
            this.lastSettedSpeedLabel.Name = "lastSettedSpeedLabel";
            this.lastSettedSpeedLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // PingReportWindow
            // 
            this.PingReportWindow.FormattingEnabled = true;
            this.PingReportWindow.Location = new System.Drawing.Point(16, 256);
            this.PingReportWindow.Name = "PingReportWindow";
            this.PingReportWindow.Size = new System.Drawing.Size(370, 134);
            this.PingReportWindow.TabIndex = 16;
            // 
            // ShowPingsFlag
            // 
            this.ShowPingsFlag.AutoSize = true;
            this.ShowPingsFlag.Location = new System.Drawing.Point(16, 233);
            this.ShowPingsFlag.Name = "ShowPingsFlag";
            this.ShowPingsFlag.Size = new System.Drawing.Size(79, 17);
            this.ShowPingsFlag.TabIndex = 17;
            this.ShowPingsFlag.Text = "show pings";
            this.ShowPingsFlag.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(293, 64);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(50, 13);
            this.label6.TabIndex = 18;
            this.label6.Text = "Time left:";
            // 
            // TimeLeftLabel
            // 
            this.TimeLeftLabel.AutoSize = true;
            this.TimeLeftLabel.Location = new System.Drawing.Point(340, 64);
            this.TimeLeftLabel.Name = "TimeLeftLabel";
            this.TimeLeftLabel.Size = new System.Drawing.Size(0, 13);
            this.TimeLeftLabel.TabIndex = 19;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.CurrentSpeedLabel);
            this.groupBox1.Controls.Add(this.RSRPLabel);
            this.groupBox1.Controls.Add(this.SINRLabel);
            this.groupBox1.Controls.Add(this.CellIDLabel);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Location = new System.Drawing.Point(16, 138);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(370, 89);
            this.groupBox1.TabIndex = 20;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Statistics";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 16);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(41, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "Cell ID:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 34);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(36, 13);
            this.label8.TabIndex = 1;
            this.label8.Text = "SINR:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 52);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(40, 13);
            this.label9.TabIndex = 2;
            this.label9.Text = "RSRP:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 70);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(76, 13);
            this.label10.TabIndex = 3;
            this.label10.Text = "Current speed:";
            // 
            // CellIDLabel
            // 
            this.CellIDLabel.AutoSize = true;
            this.CellIDLabel.Location = new System.Drawing.Point(48, 16);
            this.CellIDLabel.Margin = new System.Windows.Forms.Padding(0);
            this.CellIDLabel.Name = "CellIDLabel";
            this.CellIDLabel.Size = new System.Drawing.Size(0, 13);
            this.CellIDLabel.TabIndex = 4;
            // 
            // SINRLabel
            // 
            this.SINRLabel.AutoSize = true;
            this.SINRLabel.Location = new System.Drawing.Point(43, 34);
            this.SINRLabel.Margin = new System.Windows.Forms.Padding(0);
            this.SINRLabel.Name = "SINRLabel";
            this.SINRLabel.Size = new System.Drawing.Size(0, 13);
            this.SINRLabel.TabIndex = 5;
            // 
            // RSRPLabel
            // 
            this.RSRPLabel.AutoSize = true;
            this.RSRPLabel.Location = new System.Drawing.Point(47, 52);
            this.RSRPLabel.Margin = new System.Windows.Forms.Padding(0);
            this.RSRPLabel.Name = "RSRPLabel";
            this.RSRPLabel.Size = new System.Drawing.Size(0, 13);
            this.RSRPLabel.TabIndex = 6;
            // 
            // CurrentSpeedLabel
            // 
            this.CurrentSpeedLabel.AutoSize = true;
            this.CurrentSpeedLabel.Location = new System.Drawing.Point(83, 70);
            this.CurrentSpeedLabel.Margin = new System.Windows.Forms.Padding(0);
            this.CurrentSpeedLabel.Name = "CurrentSpeedLabel";
            this.CurrentSpeedLabel.Size = new System.Drawing.Size(0, 13);
            this.CurrentSpeedLabel.TabIndex = 7;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(400, 422);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.TimeLeftLabel);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.ShowPingsFlag);
            this.Controls.Add(this.PingReportWindow);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.DelayedStartBtn);
            this.Controls.Add(this.speedListTimer);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.isTimerUsing);
            this.Controls.Add(this.timerInterval);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.ChangeSpeedBtn);
            this.Controls.Add(this.speedList);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.passTextField);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.loginTextField);
            this.Controls.Add(this.label1);
            this.MinimumSize = this.MaximumSize;
            this.Name = "Form1";
            this.Text = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Resize += new System.EventHandler(this.Form1_Resize);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox loginTextField;
        private System.Windows.Forms.TextBox passTextField;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox speedList;
        private System.Windows.Forms.Button ChangeSpeedBtn;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.MaskedTextBox timerInterval;
        private System.Windows.Forms.CheckBox isTimerUsing;
        private System.Windows.Forms.ComboBox speedListTimer;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button DelayedStartBtn;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel lastSettedSpeedLabel;
        private System.Windows.Forms.ListBox PingReportWindow;
        private System.Windows.Forms.CheckBox ShowPingsFlag;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label TimeLeftLabel;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label CurrentSpeedLabel;
        private System.Windows.Forms.Label RSRPLabel;
        private System.Windows.Forms.Label SINRLabel;
        private System.Windows.Forms.Label CellIDLabel;
        private System.Windows.Forms.Label label10;
    }
}

